# Preliminaries

## Options available for Dependency Injection (DI)
<p>
The followings are several well-known DI frameworks for Java:

* [Spring](https://spring.io/), released in Oct 2002,
* [Guice](https://github.com/google/guice), released in March 2007,
* [Dagger-1](http://square.github.io/dagger/), released in May 2013 (deprecated on Sep 2016),
* [Dagger-2](https://github.com/google/dagger/), released in April 2015.
</p>

## Why Dagger-2
<p>
This tutorial is not supposed to convince you to use Dagger 2. Hopefully, you are already convinced, since you are here to learn it.
</p>
<p>
However, the most appealing features of Dagger 2, IMHO, are as follows:
1. No run-time overhead, everything at compile time :) (No reflection)
2. Real code generated, which can be traced during debug :)
3. Only Java classes, no .xml etc.
</p>
<p>
Besides this, there are some drawbacks as well including:
1. No dynamism
2. etc.
</p>
<p>
<b>Recommended:</b> there are several talks and articles listed as [references](#references), which can give you a comparison over other alternatives.
</p>

## Big Image
<p>

* [Setup your maven](#maven-setup)
* For each <i>Injectable</i> class, add <b>@Inject</b> before the constructor
* For each class, <i>Inject</i> all its <i>dependencies</i>:
  ** <b>preferably</b>, at constructor level: by adding all the fields as constructor input parameters and adding <i>@Inject</i> before the constructor,
  ** or at field level: by adding <i>@Inject</i> before each field (then fields cannot be <i>private final</i>),
* Add <i>Module(s)</i> (java class), where you inform Dagger how to generate instances to be injected (mapping),
  ** this mapping can be done via a single module class or distributed among several ones,
* Add </i>Component(s)</i> (java interface), where you actually define your factories to get instances
  ** in components, you need to refer to appropriate modules defining the required mapping,
* Dagger 2 generates all the boilerplate for you!
* In your code, just use the generated component class to have an instance of your <i>abstract factory</i> and go on with creating your objects.
</p>

## Maven Setup
<p>
Update <b>pom.xml</b>:

1. include <i>dagger</i> artifact in <i>dependencies</i>
```xml
<dependencies>
  <dependency>
    <groupId>com.google.dagger</groupId>
    <artifactId>dagger</artifactId>
    <version>2.20</version>
  </dependency>
</dependencies>
```
2. include <i>dagger-compiler</i> artifact as a value for <i>maven-compiler-plugin</i>
```xml
<build>
  <plugins>
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-compiler-plugin</artifactId>
      <version>3.7.0</version>
      <configuration>
        <annotationProcessorPaths>
          <path>
            <groupId>com.google.dagger</groupId>
            <artifactId>dagger-compiler</artifactId>
            <version>2.20</version>
          </path>
        </annotationProcessorPaths>
      </configuration>
    </plugin>
  </plugins>
</build>
```
</pre>

* Note that versions might be updated as you read this (better to use <i>properties</i> to include versions).
* [official reference](https://github.com/google/dagger/blob/master/README.md#installation).
</p>

# Tutorial

## Let's make a simple game

Let's make a simple game to win some money (or loose some)! The game runs for several (e.g. 10) rounds. The user starts with 0 balance. At each round, some random amount (a prime number) is transferred/deducted to/from the user's account. At the end, the user either wins money or is in debt according to the final balance. The simple game is depicted as below:

![SimpleGame](images/SimpleGame.png)

* As shown, there is *gameIt(int rounds)* method that loops for *rounds* and gets a **random number** and a **random sign** and updates the balance, accordingly:
```java
    int gameIt(int rounds) {
        int balance = 0;
        for (int i = 0; i < rounds; ++i) {
            balance += randomPrime.getNext() * randomSign.getSignAsCoefficient();
        }
        return balance;
    }
```
* randomPrime and randomSign are *dependencies* for this class, let's *inject* them in via constructor:
```java
class SimpleGame {

    private final RandomPrime randomPrime;
    private final RandomSign randomSign;

    @Inject
    public SimpleGame(RandomPrime randomPrime, RandomSign randomSign) {
        this.randomPrime = randomPrime;
        this.randomSign = randomSign;
    }
...
}
```
* Lets define the dependencies, first RandomPrime:

![RandomPrime](images/RandomPrime.png)

* The *next* method simply returns a random prime number.
* Then RandomSign:

![RandomSign](images/RandomSign.png)

* As shown, RandomSign has a *signAsCoefficient* method which returns 1 or -1 for plus and minus signs, respectively. To do this, it uses RandomPrime to generate a random sign as below:
```java
    public int getSignAsCoefficient() {
        return randomPrime.getNext() < randomPrime.getNext() ? -1 : 1;
    }
```
* Therefore, it also depends on RandomPrime. Let's inject it:
```java
class RandomSignImpl implements RandomSign {

    private final RandomPrime randomPrime;

    @Inject
    public RandomSignImpl(RandomPrime randomPrime) {
        this.randomPrime = randomPrime;
    }
...
}
```

## Modules

* So far, we have injected all the dependencies. We need to make a mapping such as below:

![DependencyMapping](images/DependencyMapping.png)

* We do this via *Modules* (**Note** that there are several ways to do this, as illustrated in the code):
* First, start with *RandomPrimeModule*, which is a java class to inform Dagger 2 how to generate an appropriate factory:
```java
import dagger.Module;
import dagger.Provides;

@Module
class RandomPrimeModule {

    @Provides
    RandomPrime provideRandomPrime(RandomPrimeImpl randomPrime) {
        return randomPrime;
    }
}
```
This is enough for the appropriate factory to be created automatically.
* Then, continue with *RandomSignModule* (Note that how RandomPrimeModule is included):
```java
import dagger.Module;
import dagger.Provides;

@Module
class RandomSignModule {
    @Provides
    RandomSign provideRandomSign(RandomSignImpl randomSign) {
        return randomSign;
    }
}
```
* Finally, there is not much left for *SimpleGameModule* except including other modules, where mappings to real implementations are done.
```java
import dagger.Module;
@Module(includes = { RandomPrimeModule.class, RandomSignModule.class })
class SimpleGameModule {
}
```

## Component

* All required modules are created, so let's make the *component* which creates an *Abstract factory* that can be used in our code to generate/get instances:
```java
import dagger.Component;

@Component(modules = SimpleGameModule.class)
public interface GameMakerComponent {
    SimpleGame getSimpleGame();
    RandomPrime getRandomPrime(); // not really needed for this example, but added only for educational purposes
    RandomSign getRandomSign(); // not really needed for this example, but added only for educational purposes
}
```
## Application

* Congratulations! We are done with *Injecting dependencies*! Let's make our application:
```java
public class AppWithInjection {

    public static void main(String[] args) {

        final GameMakerComponent factory = DaggerGameMakerComponent.builder().build();

        final int result = factory.getSimpleGame().gameIt(10);

        System.out.println((result < 0 ? "You're in debt!" : "You won money!") + ", amount=" + Math.abs(result));
    }
}
```

* WELL DONE! you made your application by utilizing dagger-2 :) BTW, have you seen any ''new'' in the code? ;-)
* Note that *DaggerGameMakerComponent* is instantly created for you by dagger 2.

## Extra

Here are some more features provided by Dagger-2.

### Named
<details>
<summary>
What if I'd like to have different implementations for each of the injected instances?  Just <i>name</i> it, my lord!
</summary>

* For example, just consider SimpleGame needs to have an implementation of RandomPrime which returns each time a random prime number among the first 1000 primes. However, a lighter version is enough for RandomSign, e.g. returning a random prime among the first 10 primes.
* Just update the RandomPrimeModule as follows:
```java
import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
class RandomPrimeModule {
    @Provides
    @Named("default")
    RandomPrime provideDefaultRandomPrime() {
        return new RandomPrimeImpl();
    }

    @Provides
    @Named("light")
    RandomPrime provideLightRandomPrime() {
        return new RandomPrime2ndImpl();
    }
}
```

Then, update the injections by appropriate *names*:
* SimpleGame, add the name in constructor:
```java
import javax.inject.Named;

class SimpleGame {

    private final RandomPrime randomPrime;
    private final RandomSign randomSign;

    @Inject
    public SimpleGame(@Named("default") RandomPrime randomPrime, RandomSign randomSign) {
        this.randomPrime = randomPrime;
        this.randomSign = randomSign;
    }
...
}
```
* The same for RandomSign:
```java
import javax.inject.Named;

class RandomSignImpl implements RandomSign {

    private final RandomPrime randomPrime;

    @Inject
    public RandomSignImpl(@Named("light") RandomPrime randomPrime) {
        this.randomPrime = randomPrime;
    }
...
}
```
* DONE! here is the new mapping:

![Named](images/Named.png)
</details>

### Singleton

<details>
<summary><i>Singleton</i> is one of the [scopes](http://frogermcs.github.io/dependency-injection-with-dagger-2-custom-scopes/) that can be define with Dagger-2. It ensures to have the same instances to be returned per <i>component</i> instance (means per factory).</summary>

Easy! Just add @Singleton to
1. *classes* that must be created with only one instance,
2. *provide* methods in *modules*,
3. *component* where you get your factory form.

* Refer to the code to see an example.
* Compare how instances are the same or not whether @Singleton is presented or not as shown below: 

![Singleton](images/Singleton.png)
</details>

### Reusable

<details>
<summary>limiting instantiation with no guarantee for delivering the same object.</summary>

* *@Reusable* is a nice feature of Dagger to limit the number of instantiated objects without any guarantee for having the same object.
* It seems more interesting for Android developers, where instantiation could be pretty expensive due to limited resources.
* Each component <i>caches</i> the instantiated objects to help <i>reuse</i>.

</details>

### Binds

<details>
<summary>An alternative for @Provides</summary>

<i>Binding</i> could be used as an alternative for <i>Providing</i>, as demonstrated in the followings:

* Whenever the implementation of a <i>'provide'</i> method is straightforward, we can use <b>@Binds</b> to declare it as an abstract method <b>without any implementation</b> in the corresponding module. Accordingly, the module class will be abstract as well. Dagger will generate the appropriate implementation there.
* For example, we use @Binds for <i>provideRandomSign</i> method, since the implementation of RandomSign is pretty obvious. Note that the whole module is become an abstract class as well.
```java
import dagger.Binds;
import dagger.Module;

@Module
abstract class RandomSignModule {
    @Binds
    abstract RandomSign provideRandomSign(RandomSignImpl randomSign);
}
```
* <b>Note:</b> @Binds and @Provides cannot appear in the same module.
* [Official reference for @Binds](https://google.github.io/dagger/faq#binds), e.g. the difference between @Binds and @Provides is explained there.

</details>

### BindsInstance

<details>
<summary>In case, if some parameters should be provided at object instantiation, @BindsInstance comes handy there.</summary>

Let's continue with our simple game:

* We need an "Evaluation" object which gets the result of the game and generates an appropriate String to be displayed.
* This class has only one method: <i>getEvaluation</i> described below:
```java
    String getEvaluation(int balance) {
        if (balance < 0) {
            return sadMessage + " You're in debt of " + Math.abs(balance);
        }
        if (balance > 0) {
            final int tax = (int)(balance * taxationRate);
            return happyMessage + " You won the amount of " + Math.abs(balance) + ", and you should pay the tax: " + tax;
        }
        return "You neither won nor lost! Balance is 0";
    }
```
* In this case, it also tells you how much tax you should pay for your win.
* As shown, there are three fields that should be initialized at the time of creating an object:
  1. taxationRate
  2. happyMessage
  3. sadMessage
* Easy! just put them in the constructor!
```java
class EvaluateResult {

    private final String happyMessage;
    private final String sadMessage;
    private final double taxationRate;

    public EvaluateResult(String happyMessage, String sadMessage, double taxationRate) {
        this.happyMessage = happyMessage;
        this.sadMessage = sadMessage;
        this.taxationRate = taxationRate;
    }
```

* So far, so good! the only remaining is to inform Dagger to get them at build time. This can be done via @BindsInstance at the *component level*. For example, here is the code for adding <i>taxationRate</i>:
```java
import dagger.BindsInstance;
import dagger.Component;

@Component(modules = EvaluateResultModule.class)
public interface EvaluationComponent {

    EvaluateResult getEvaulateResult();

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder taxationRate(double taxationRate);

        ...

        EvaluationComponent build();
    }
}
```
* As demonstrated, we just updated the Builder for the component to get the <i>taxationRate</i> at build time.
* Since such kind of methods are bound based on the *type*, therefore, we need to solve any ambiguity of binding for types with more than one instances. For example, we should separate between <i>happyMessage</i> and <i>sadMessage</i> instances, which are both String, via [Named(<i>"name"</i>)](#named) term:
```java
import dagger.BindsInstance;
import dagger.Component;

@Component(modules = EvaluateResultModule.class)
public interface EvaluationComponent {

    EvaluateResult getEvaulateResult();

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder taxationRate(double taxationRate);

        @BindsInstance
        Builder happyMessage(@Named("happy") String happyMessage);

        @BindsInstance
        Builder sadMessage(@Named("sad") String sadMessage);

        EvaluationComponent build();
    }
}
```
* The corresponding *module* also uses appropriate names for creating instances:
```java
import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
class EvaluateResultModule {

    @Provides
    EvaluateResult provideEvaluateResult(@Named("happy") String happyMessage, @Named("sad") String sadMessage, double taxationRate) {
        return new EvaluateResult(happyMessage, sadMessage, taxationRate);
    }
}
```
* Well done! we are ready to create our instances with appropriate settings in the application:
```java
// in the application, e.g. main
        ...

        final EvaluationComponent evaluationFactory
                = DaggerEvaluationComponent.builder().
                happyMessage("Congrats!").
                sadMessage("Sorry!").
                taxationRate(0.21d).
                build();
        final EvaluateResult evaulateResult = evaluationFactory.getEvaulateResult();

        ...
```

</details>

### BindsOptionalOf

<details>
<summary>Under construction</summary>
</details>

### Lazy Injections

<details>
<summary>Since I am already <i>lazy</i>, I just quote it from [the official webpage](https://google.github.io/dagger/users-guide) ;-)</summary>

<blockquote cite="https://google.github.io/dagger/users-guide">
Sometimes you need an object to be instantiated lazily.

* For any binding T, you can create a Lazy<T> which defers instantiation until the first call to Lazy<T>’s get() method.
  * If T is a singleton, then Lazy<T> will be the same instance for all injections within the ObjectGraph.
  * Otherwise, each injection site will get its own Lazy<T> instance.
* Regardless, subsequent calls to any given instance of Lazy<T> will return the same underlying instance of T.
</blockquote>

The example from the same source:
```java
class GrindingCoffeeMaker {
  @Inject Lazy<Grinder> lazyGrinder;

  public void brew() {
    while (needsGrinding()) {
      // Grinder created once on first call to .get() and cached.
      lazyGrinder.get().grind();
    }
  }
}
```
</details>

## References

1. [Dagger official webpage](https://google.github.io/dagger/)
   * [Dagger official user's guide](https://google.github.io/dagger/users-guide)
   * [Dagger API](https://google.github.io/dagger/api/latest/)
   * [on GitHub](https://github.com/google/dagger)
   * [Dagger official documentation](https://docs.google.com/document/d/1fwg-NsMKYtYxeEWe82rISIHjNrtdqonfiHgp8-PQ7m8/edit)
2. [A good answer to ''why dagger 2?'' on StackOverflow](https://stackoverflow.com/questions/39688830/why-use-develop-guice-when-you-have-spring-and-dagger)
3. [Dagger 2: Scopes](http://frogermcs.github.io/dependency-injection-with-dagger-2-custom-scopes/)
4. [A guide from CodePath](https://guides.codepath.com/android/Dependency-Injection-with-Dagger-2)
6. [A guide for getting started](https://developerlife.com/2018/10/21/getting-started-with-dagger2/)
7. Talks
   * [By Gregory Kick](https://www.youtube.com/watch?v=oK_XtfXPkqw&feature=youtu.be) ([Slides](https://docs.google.com/presentation/d/1fby5VeGU9CN8zjw4lAb2QPPsKRxx6mSwCe9q7ECNSJQ/pub?start=false&loop=false&delayms=3000#slide=id.p))
   * [By Jake Wharton](https://www.youtube.com/watch?v=plK0zyRLIP8)
</p>