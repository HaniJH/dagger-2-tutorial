package com.gilmard.di.dagger;

/**
 * Generates a random sign of either + or -.
 */
interface RandomSign {
    /**
     * @return +1 and -1 for + and -, respectively.
     */
    int getSignAsCoefficient();
}
