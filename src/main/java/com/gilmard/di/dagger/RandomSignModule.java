package com.gilmard.di.dagger;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

// version 1
@Module
abstract class RandomSignModule {
    @Binds
    abstract RandomSign provideRandomSign(RandomSignImpl randomSign);
}

// version 2
//@Module (includes = {RandomPrimeModule.class})
//class RandomSignModule {
//    @Provides
//    RandomSign provideRandomSign(@Named("light") RandomPrime randomPrime) {
//        return new RandomSignImpl(randomPrime);
//    }
//}

// version 3
//@Module
//class RandomSignModule {
//    @Provides
//    RandomSign provideRandomSign() {
//        return new RandomSignImpl(new RandomPrimeImpl());
//    }
//}
