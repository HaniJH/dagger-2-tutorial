package com.gilmard.di.dagger;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class RandomPrimeModule {

    @Provides
    @Singleton
    @Named("default")
    static RandomPrime provideDefaultRandomPrime(RandomPrimeImpl randomPrime) {
        return randomPrime;
    }

    @Provides
    @Singleton
    @Named("light")
    static RandomPrime provideLightRandomPrime(RandomPrime2ndImpl randomPrime) {
        return randomPrime;
    }
}
