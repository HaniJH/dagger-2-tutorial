package com.gilmard.di.dagger;

import javax.inject.Named;

import dagger.BindsInstance;
import dagger.Component;

@Component(modules = EvaluateResultModule.class)
public interface EvaluationComponent {

    EvaluateResult getEvaulateResult();

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder happyMessage(@Named("happy") String happyMessage);

        @BindsInstance
        Builder sadMessage(@Named("sad") String sadMessage);

        @BindsInstance
        Builder taxationRate(double taxationRate);

        EvaluationComponent build();
    }
}
