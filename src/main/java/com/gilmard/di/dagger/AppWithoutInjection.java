package com.gilmard.di.dagger;

import java.util.List;

public class AppWithoutInjection {

    public static void main(String[] args) {

        final RandomPrime randomPrime = new RandomPrimeImpl();
        final RandomSign randomSign = new RandomSignImpl(randomPrime);
        final SimpleGame simpleGame = new SimpleGame(randomPrime, randomSign);

        final List<List<Integer>> result = simpleGame.gameIt(10);
        System.out.println(result);
        System.out.println(result.get(result.size() - 1).get(1) < 0 ? "You're in debt!" : "You won money!");
    }
}
