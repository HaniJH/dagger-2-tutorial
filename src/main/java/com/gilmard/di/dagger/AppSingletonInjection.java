package com.gilmard.di.dagger;

public class AppSingletonInjection {

    public static void main(String[] args) {

        final GameMakerComponent component = DaggerGameMakerComponent.builder().build();

        final SimpleGame simpleGame1 = component.getSimpleGame();
        final SimpleGame simpleGame2 = component.getSimpleGame();
        final RandomPrime randomPrime1 = component.getDefaultRandomPrime();
        final RandomPrime randomPrime2 = component.getDefaultRandomPrime();
        final RandomPrime randomPrime3 = component.getLightRandomPrime();
        final RandomPrime randomPrime4 = component.getLightRandomPrime();
        final RandomSign randomSign1 = component.getRandomSign();
        final RandomSign randomSign2 = component.getRandomSign();

        System.out.println(simpleGame1.toString());
        System.out.println(simpleGame2.toString());
        System.out.println("");
        System.out.println(simpleGame1.getRandomPrime().toString());
        System.out.println(simpleGame2.getRandomPrime().toString());
        System.out.println(randomPrime1.toString());
        System.out.println(randomPrime2.toString());
        System.out.println("");
        System.out.println(randomSign1.toString());
        System.out.println(randomSign2.toString());
        System.out.println("");
        System.out.println(randomPrime3.toString());
        System.out.println(randomPrime4.toString());
        System.out.println(((RandomSignImpl)randomSign1).getRandomPrime().toString());
        System.out.println(((RandomSignImpl)randomSign2).getRandomPrime().toString());

    }
}
