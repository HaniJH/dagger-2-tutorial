package com.gilmard.di.dagger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * A simple game of accumulation by chance.
 */
@Singleton
class SimpleGame {

    private final RandomPrime randomPrime;
    private final RandomSign randomSign;

    @Inject
    public SimpleGame(@Named("default") RandomPrime randomPrime, RandomSign randomSign) {
        this.randomPrime = randomPrime;
        this.randomSign = randomSign;
    }

    public RandomPrime getRandomPrime() {
        return randomPrime;
    }

    List<List<Integer>> gameIt(int rounds) {
        final List<List<Integer>> result = new ArrayList<List<Integer>>();
        int balance = 0;
        for (int i = 0; i < rounds; ++i) {
            final int current = randomPrime.getNext() * randomSign.getSignAsCoefficient();
            result.add(Arrays.asList(current, balance));
            balance += current;
        }
        result.add(Arrays.asList(0, balance));
        return result;
    }
}
