package com.gilmard.di.dagger;

/**
 * Generates random prime numbers.
 */
interface RandomPrime {
    /**
     * @return the next random prime number.
     */
    int getNext();
}
