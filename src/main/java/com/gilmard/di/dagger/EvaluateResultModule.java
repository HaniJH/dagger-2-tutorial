package com.gilmard.di.dagger;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class EvaluateResultModule {

    @Provides
    EvaluateResult provideEvaluateResult(@Named("happy") String happyMessage, @Named("sad") String sadMessage, double taxationRate) {
        return new EvaluateResult(happyMessage, sadMessage, taxationRate);
    }
}
