package com.gilmard.di.dagger;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

// version 1
//@Module (includes = RandomSignModule.class)
//class SimpleGameModule {
//    @Provides
//    @Named("default")
//    @Singleton
//    RandomPrime provideDefaultRandomPrime() {
//        return new RandomPrimeImpl();
//    }
//    @Provides
//    @Named("light")
//    @Singleton
//    RandomPrime provideLightRandomPrime() {
//        return new RandomPrime2ndImpl();
//    }
//}

// version 2
@Module(includes = { RandomPrimeModule.class, RandomSignModule.class })
class SimpleGameModule {
}
