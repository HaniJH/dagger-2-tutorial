package com.gilmard.di.dagger;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;

//@Component(modules = { RandomPrime2ndModule.class, RandomSignModule.class })
@Singleton
@Component(modules = SimpleGameModule.class)
public interface GameMakerComponent {

    SimpleGame getSimpleGame();

    @Named("default")
    RandomPrime getDefaultRandomPrime();

    @Named("light")
    RandomPrime getLightRandomPrime();

    RandomSign getRandomSign();
}
