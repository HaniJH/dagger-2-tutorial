package com.gilmard.di.dagger;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class RandomPrime2ndImpl implements RandomPrime {

    private static final List<Integer> FIRST_10_PRIME_NUMBERS = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);

    @Inject
    public RandomPrime2ndImpl() {
    }

    public int getNext() {
        return FIRST_10_PRIME_NUMBERS.get(new Random().nextInt(FIRST_10_PRIME_NUMBERS.size()));
    }
}
