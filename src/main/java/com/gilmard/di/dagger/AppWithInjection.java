package com.gilmard.di.dagger;

import java.util.List;

public class AppWithInjection {

    public static void main(String[] args) {

        final GameMakerComponent gameMakerFactory = DaggerGameMakerComponent.builder().build();
        final List<List<Integer>> result = gameMakerFactory.getSimpleGame().gameIt(10);
        System.out.println(result);
        final int amount = result.get(result.size() - 1).get(1);

        final EvaluationComponent evaluationFactory
                = DaggerEvaluationComponent.builder().
                happyMessage("Congrats!").
                sadMessage("Sorry!").
                taxationRate(0.21d).
                build();
        final EvaluateResult evaulateResult = evaluationFactory.getEvaulateResult();
        System.out.println(evaulateResult.getEvaluation(amount));

        // to check if the same instance are created by @Singletone
        final SimpleGame simpleGame = gameMakerFactory.getSimpleGame();
        final SimpleGame secondSimpleGame = gameMakerFactory.getSimpleGame();
    }

}
