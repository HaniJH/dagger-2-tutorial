package com.gilmard.di.dagger;

/**
 * To evaluate the result.
 */
class EvaluateResult {

    private final String happyMessage;
    private final String sadMessage;
    private final double taxationRate;

    public EvaluateResult(String happyMessage, String sadMessage, double taxationRate) {
        this.happyMessage = happyMessage;
        this.sadMessage = sadMessage;
        this.taxationRate = taxationRate;
    }

    /**
     * @return an evaluated string based on the balance.
     */
    String getEvaluation(int balance) {
        if (balance < 0) {
            return sadMessage + " You're in debt of " + Math.abs(balance);
        }
        if (balance > 0) {
            int tax = (int)(balance * taxationRate);
            return happyMessage + " You won the amount of " + Math.abs(balance) + ", and you should pay the tax: " + tax;
        }
        return "You neither won nor lost! Balance is 0";
    }
}
