package com.gilmard.di.dagger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Implements {@link RandomSign}.
 */
@Singleton
class RandomSignImpl implements RandomSign {

    private final RandomPrime randomPrime;

    @Inject
    public RandomSignImpl(@Named("light") RandomPrime randomPrime) {
        this.randomPrime = randomPrime;
    }

    public RandomPrime getRandomPrime() {
        return randomPrime;
    }

    public int getSignAsCoefficient() {
        return randomPrime.getNext() < randomPrime.getNext() ? -1 : 1;
    }
}
